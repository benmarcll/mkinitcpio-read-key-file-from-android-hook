# mkinitcpio-read-key-file-from-android-hook

## Requirements
- adb has to be installed and run at least once: 
    - turn on `USB debugging` on ur phone,
    - connect phone with usb,
    - type `adb devices` into terminal
    - allow prompt

## Customization
- To specify the key file location on your phone, edit: `mkinitcpio-read-key-file-from-android-hook`, and set `key_file` variable, default is: `/tmp/luks_key`.
- ADB's private and public key file is in `/root/.android/adbkey` and `/root/.android/adbkey.pub` in my case, you can edit them in `mkinitcpio-read-key-file-from-android-hook`.
- By default, key read from phone will be at `/tmp/luks_key`. You can customize that one too, but be careful; edit `/etc/default/grub` afterwards.

## Getting started
- pull files, and put
    - `mkinitcpio-read-key-file-from-android-hook` to `/etc/initcpio/hooks/mkinitcpio-read-key-file-from-android`
    - `mkinitcpio-read-key-file-from-android-install` to `/etc/initcpio/install/mkinitcpio-read-key-file-from-android`
    - add `adb` to `/etc/mkinitcpio.conf` `BINARIES` array
    - add `~/.android/adbkey` and `~/.android/adbkey.pub` to `/etc/mkinitcpio.conf` `FILES` array
    - add `read_luks_key_from_android` to `/etc/mkinitcpio.conf` `HOOKS` array ** JUST BEFORE ** `encrypt` hook
    - generate initramfs (`mkinitcpio -P`)
    - add `cryptkey=rootfs:/tmp/luks_key` into `/dev/default/grub` `GRUB_CMDLINE_LINUX_DEFAULT` ** AFTER ** `cryptdevice=.....`
    - generate grub config (`grub-mkconfig -o /boot/grub/grub.cfg`)